let FAClient = null;
let recordId = null;
let linesGlobal = null;
let productList = null;

// aHR0cDovL2xvY2FsaG9zdDo1MDAw localhost:5000
//aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3Byb2R1Y3RzLWxpc3Q

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3Byb2R1Y3RzLWxpc3Qv`,
};

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

function startupService() {
    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });


    FAClient.on("openProducts", (data) => {
        console.log(data);
        recordId = data.id;
        FAClient.listEntityValues(
            {
                entity: "line_item",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [data.id],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                linesGlobal = lines;
                FAClient.open();
                FAClient.listEntityValues({entity: "product"}, (data) => {
                    productList = data;
                    generateList({products: data, lines})
                })
            });
    });

    let searchEl = document.getElementById('search');
    searchEl.addEventListener("keyup", (event) => {
        let value = event.target.value.toLowerCase();
        if (productList) {
            let filteredList = productList.filter(prod => prod.field_values.product_field0.display_value.toLowerCase().includes(value))
            generateList({products:filteredList, lines: linesGlobal})
            console.log(filteredList)
        }
    });

}

function generateList({products, lines}) {
    let list = document.getElementById('order-list');
    console.log(list);
    list.innerHTML = '';
    products.map(product => {
        let lineMatch = lines.find(line => line.field_values.line_item_field0.value === product.id);
        let qty = 0;
        if (lineMatch) {
            qty = lineMatch.field_values.line_item_field2.display_value;
        }
        let fieldValues = product.field_values;
        let priceField = fieldValues.product_field9;
        let priceFormated = priceField.formatted_value;
        let productName = fieldValues.product_field0.display_value;
        let productImg = fieldValues.product_field13.display_value;
        let inventory = fieldValues.product_field14.formatted_value || 0;

        let liEl = document.createElement('li');
        let thumbnail = document.createElement('img');
        thumbnail.setAttribute("src", productImg);
        let heather4 = document.createElement('h4');
        let heather4Text = document.createTextNode(`${productName} ${priceFormated}`);
        heather4.appendChild(heather4Text);
        let heather5 = document.createElement('h5');
        let heather5Text = document.createTextNode(`In Stock: ${inventory}`);
        heather5.appendChild(heather5Text);
        let input = document.createElement('input');
        input.setAttribute("type", "number");
        input.setAttribute("min", "0");
        input.setAttribute("value", `${qty}`);
        input.setAttribute("name", fieldValues.product_field0.value);
        input.addEventListener("change", (event) => {
            console.log(product.id);
            console.log(product.seq_id);
            console.log(productName);
            console.log(event);
            let qty = event.target.valueAsNumber;
            console.log(linesGlobal);
            let lineFound = linesGlobal.find(line => line.field_values.line_item_field0.value === product.id);
            if (lineFound) {
                if (qty > 0) {
                    console.log('it is running')
                    updateQty({id: lineFound.id, qty});
                    updateRecord();
                } else {
                    deleteEntity(lineFound.id)
                    linesGlobal = linesGlobal.filter(line => line.field_values.line_item_field0.value !== product.id);
                    lineFound = null;
                    updateRecord();
                }
            } else {
                if (qty > 0) {
                    addItem({product, qty});
                    updateRecord();
                }
            }

        })
        let inputContainer = document.createElement('div');
        inputContainer.appendChild(input);
        liEl.appendChild(thumbnail);
        liEl.appendChild(heather4);
        liEl.appendChild(heather5);
        liEl.appendChild(inputContainer);
        list.appendChild(liEl);
    });
}

function deleteEntity(id) {
    let updatePayload = {
        entity: "line_item",
        id: id,
        field_values: { deleted: true }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
        updateRecord();
    });
}


function updateQty({id, qty}) {
    let updatePayload = {
        entity: "line_item",
        id: id,
        field_values: { line_item_field2: qty }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
        updateRecord();
    })
    console.log(id, qty);
}

function addItem({product, qty}) {
    let payload = {
        entity: "line_item",
        field_values: { line_item_field0 : product.id , line_item_field2: qty, parent_entity_reference_id: "" }
    };
    payload.field_values.parent_entity_reference_id = recordId;
    console.log(payload);
    let res = null;
    FAClient.createEntity(payload, (data) => {
        console.log(data);
        updateRecord();
        linesGlobal.push(data.entity_value)
    })
}

function updateRecord(id=recordId) {
    let updatePayload = {
        entity: "order",
        id: id,
        field_values: {}
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}
